FROM gitpod/workspace-full:latest

USER gitpod
RUN /bin/bash -l -c "\
       source /home/gitpod/.nvm/nvm.sh \
    && nvm install 13 \
    && nvm use 13 \
    && npm install -g pnpm"

USER root


# Install custom tools, runtime, etc. using apt-get
# For example, the command below would install "bastet" - a command line tetris clone:
#
# RUN sudo apt-get -q update && #     sudo apt-get install -yq bastet && #     sudo rm -rf /var/lib/apt/lists/*
#
# More information: https://www.gitpod.io/docs/42_config_docker/
