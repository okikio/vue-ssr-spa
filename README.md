# vue-ssr-spa

[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/okikio/vue-ssr-spa) 

Using vue server side rendering (ssr) together with vue as a single page app (spa) to allow initial load of a page to be served as pre-rendered HTML but all subsequent request from that page will be JSON, treating the app as a SPA.