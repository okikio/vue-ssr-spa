const vue_ssr = require("vue-server-renderer").createRenderer();
const compress = require("fastify-compress");
const static = require("fastify-static");
const cors = require("fastify-cors");
const nunjucks = require("nunjucks");
const fastify = require("fastify");
const logger = require("morgan");
const path = require("path");
const vue = require("vue");
const fs = require("fs");

// Normalize a port into a number, string, or false.
let normalizePort = val => {
    let port = parseInt(val, 10);
    if (isNaN(port)) { return val; } // Named pipe
    if (port >= 0) { return port; } // Port number
    return false;
};

let HOST = 'localhost';
let root = path.join(__dirname, 'public');
let PORT = normalizePort(process.env.PORT || 3000);

let maxAge = 0 * 1000 * 60 * 60 * 24 * 7;
let app = fastify({
    logger: null && {
        prettyPrint: {
            translateTime: "hh:MM:ss TT",
            ignore: 'pid,hostname,reqId' // --ignore
        }
    },
    ignoreTrailingSlash: true,
    caseSensitive: false
});

app.use(logger('dev')) // Simple HTTP Logging
   .register(compress) // Compress/GZIP/Brotil Server

   // Apply CORS
   .register(cors, {
        methods: ['GET', 'PUT', 'POST'],
        cacheControl: true, maxAge,
        preflightContinue: true,
        preflight: true
    })

   // Server Static File
   .register(static, { cacheControl: true, maxAge, root });

nunjucks.configure('layouts', { autoescape: false });

// Routes and the pages to render
app.get("/", (req, res) => {
    let template = fs.readFile(`pages/index/template.vue`, 'utf-8', (err, template) => {
        if (err) throw err; 
        const app = new vue({
            data: {
                url: req.raw.url
            },
            template: template
        });

        vue_ssr.renderToString(app, (err, html) => {
            if (err) throw err;
            let layout = nunjucks.render('standard.html', { style: '', app: html, script: '' });
            
            res
                .type('text/html')
                .send(layout);
        });
    });
});

// Error handling
app.setNotFoundHandler((req, res) => {
    res
        .code(404)
        .type(req.headers["content-type"] || 'text/plain')
        .send("Error 404 -- ");
});

app.setErrorHandler((err, req, res) => {
    let statusCode = err.statusCode >= 400 ? err.statusCode : 500;
    req.log.warn(err);
    res
        .code(statusCode)
        .type(req.headers["content-type"] || 'text/plain')
        .send(statusCode >= 500 ? "Internal server error" : err.message);
});

app.listen(PORT, HOST, err => {
    if (err) app.log.error(err);
    app.log.info("Server listening on port", PORT);
});